package controller;

import java.time.LocalDateTime;

import API.IManager;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static Manager manager = new Manager();

  
    public static void cargar(String dataTrips){
    	manager.cargar(dataTrips);
    }
    
    public static void cargarStations() {
    	manager.cargarStations();
    }
    
    public int size()
    {
    	return manager.getTrips().size();
	}
  
    public static void cargarN(String dataTrips, int n)
    {
    	manager.cargarN(dataTrips,n);
    }
    
    public static void crearColaP(LocalDateTime fInicial, LocalDateTime fFinal) {
    	manager.crearColaP(fInicial, fFinal);
    }
    
    public static void crearMonticuloCP(LocalDateTime fInicial, LocalDateTime fFinal) {
    	manager.crearMonticuloCP(fInicial, fFinal);
    }
}
