package model.vo;

import java.time.LocalDateTime;

import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.MaxPQ;
import model.data_structures.OrderedArrayMaxPQ;
import model.logic.Manager;

public class ManejadorBicicletas {

	public ManejadorBicicletas() {}
	
	public static OrderedArrayMaxPQ<Bike> crearColaP (LocalDateTime fInicial, LocalDateTime fFinal) {
		LinkedList<Trip> lista = Manager.ViajesEnPeriodoDeTiempo(fInicial, fFinal);
		LinkedList<Bike> bikes = Manager.cargarBikes(lista);
		OrderedArrayMaxPQ<Bike> or = new OrderedArrayMaxPQ<Bike>(bikes.size());
		
		ListIterator<Bike> ite = bikes.iterator();
		while(ite.hasNext()){
			Bike b = ite.next();
			or.insert(b);
		}
		
		return or;
	}
	
	
	public static MaxPQ<Bike> crearMonticuloCP (LocalDateTime fInicial, LocalDateTime fFinal) {
		LinkedList<Trip> lista = Manager.ViajesEnPeriodoDeTiempo(fInicial, fFinal);
		LinkedList<Bike> bikes = Manager.cargarBikes(lista);
		
		int i =0;
		Bike[] arr = new Bike[bikes.size()];
		
		ListIterator<Bike> ite = bikes.iterator();
		while(ite.hasNext()){
			Bike b = ite.next();
			arr[i] = b;
			i++;
		}
		
		MaxPQ<Bike> mpq = new MaxPQ<Bike> (arr);
		
		return mpq;

	}
	
}
