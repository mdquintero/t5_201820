package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

    private int bikeId;

    private int totalDistance;


    public Bike(int bikeId, int totalDistance ) {
        this.bikeId = bikeId;

        this.totalDistance = totalDistance;

    }

    @Override
    public int compareTo(Bike o) {
    	if(this.totalDistance>o.getTotalDistance())
    		return -1;
    	else
    		return 1;
    }
    

    public int getBikeId() {
        return bikeId;
    }


    public int getTotalDistance() {
        return totalDistance;
    }
    
    public void addDistance(double d)
    {
    	totalDistance+=d;
    }

}
