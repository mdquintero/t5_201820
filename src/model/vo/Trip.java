package model.vo;

import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";
    public final static String FIN = "termina";
    public final static String INICIO = "inicia";
    public enum Tipo
    {
    	FIN,INICIO,NULL;
    }
    
    private Tipo tipo;
    private int tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private int endStationId;
    private String gender;
    private double distance;

    
    public Trip(int tripId, LocalDateTime startTime, LocalDateTime stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.gender = gender;
        distance = 0;
        tipo = Tipo.NULL;
    }

    @Override
    public int compareTo(Trip o) {
    	
    	
    	if(o!=null)
    	{
    		if(this.startTime.isBefore(o.stopTime))
    			return -1;
    		else if(this.startTime.isAfter(o.stopTime))
    			return 1;
    		else
    			return 0;
        	}
        	return 98;
    	}
    
 public int compareToById(Trip o) {
    	
    	
    	if(o!=null)
    	{
    		if(this.bikeId<o.bikeId)
    			return -1;
    		else if(this.bikeId>o.bikeId)
    			return 1;
    		else
    			return 0;
        	}
        	return 98;
    	}
		
	
    
    
    public int getTripId() {
        return tripId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
    
    public double getDistance()
    {
    	return distance;
    }
    
    public void setDistance(double pDistance) {
    	this.distance = pDistance;
    }
    
    public void setTipo(Tipo t)
    {
    	this.tipo=t;
    }
    
    public Tipo getTipo()
    {
    	return tipo;
    }
}
