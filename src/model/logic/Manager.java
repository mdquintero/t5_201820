package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.lang.Math;
import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.vo.Bike;
import model.vo.ManejadorBicicletas;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class Manager implements IManager {

	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = rutaGeneral+"Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = rutaGeneral+"Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = rutaGeneral+"Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = rutaGeneral+"Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = rutaGeneral+"Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv";

	private static LinkedList<Trip> linkTrips = new LinkedList<Trip>();

	private static LinkedList<Bike> listaBikes = new LinkedList<Bike>();
	
	private static LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();


	public LinkedList<Trip> getTrips()
	{
		return linkTrips;
	}
	
	//M�TODOS DE CARGA
	
	/**
	 * Carga las estaciones de q3 y q4
	 */
	public void cargarStations() {
		//cargar stations
				try{
					FileReader fr = new FileReader(new File(STATIONS_Q3_Q4));
					CSVReader br = new CSVReader(fr);

					String[] line = br.readNext();
					//Se avanza de nuevo porque la l�nea anterior no contiene datos
					line = br.readNext();

					while(line!=null)
					{
						String[] fecha = (line[6].replace(' ', '/').replace(':', '/')).split("/");
						int tamano = fecha.length;
						Integer[]fecha2 = new Integer[tamano];
						for(int i = 0; i< fecha2.length; i++)
						{
							fecha2[i] = Integer.parseInt(fecha[i]);
						}
						LocalDateTime ini = LocalDateTime.of(fecha2[2], fecha2[0], fecha2[1],fecha2[3],fecha2[4],tamano==6 ? fecha2[5]:00);
						Station s = new Station(Integer.parseInt(line[0]), line[1],ini, Double.parseDouble(line[3]),Double.parseDouble(line[4]));


						listaEncadenadaStations.add(s);

						line = br.readNext();
					}

					br.close();
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
	}
	
	/**
	 * Carga N Trips
	 */
	public void cargarN(String rutaTrips, int n) {

		//cargar trips
		try{
			FileReader fr = new FileReader(new File(rutaTrips));
			CSVReader br = new CSVReader(fr);
			int contador=0;
			Trip t=null;


			String[] line = br.readNext();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readNext();
			while(line!=null&&contador<n)
			{


				String[] fechaInicio = (line[1].replace(' ', '/').replace(':', '/')).split("/");
				int tamano = fechaInicio.length;
				Integer[]fechaInicio2 = new Integer[tamano];
				String[] fechaFin = (line[2].replace(' ', '/').replace(':', '/')).split("/");
				Integer[]fechaFin2 = new Integer[tamano];

				for(int i = 0; i< fechaFin2.length; i++)
				{
					fechaInicio2[i] = Integer.parseInt(fechaInicio[i]);
					fechaFin2[i] = Integer.parseInt(fechaFin[i]);
				}

				LocalDateTime ini = LocalDateTime.of(fechaInicio2[2], fechaInicio2[0], fechaInicio2[1],fechaInicio2[3],fechaInicio2[4],(tamano==6 ? fechaInicio2[5]:00));
				LocalDateTime fini = LocalDateTime.of(fechaFin2[2], fechaFin2[0], fechaFin2[1],fechaFin2[3],fechaFin2[4],tamano==6 ? fechaFin2[5]:00);
				if(line[10]==null || line[10].equals(""))
				{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), Trip.UNKNOWN);

				}
				else{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), line[10]);

				}
				linkTrips.add(t);
				line = br.readNext();
				contador++;
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
	/**
	 * Carga todo
	 * @param rutaTrips la ruta
	 */
	public void cargar(String rutaTrips) {

		//cargar trips
		try{
			FileReader fr = new FileReader(new File(rutaTrips));
			CSVReader br = new CSVReader(fr);
			Trip t=null;


			String[] line = br.readNext();
			//Se avanza de nuevo porque la l�nea anterior no contiene datos
			line = br.readNext();
			while(line!=null)
			{


				String[] fechaInicio = (line[1].replace(' ', '/').replace(':', '/')).split("/");
				int tamano = fechaInicio.length;
				Integer[]fechaInicio2 = new Integer[tamano];
				String[] fechaFin = (line[2].replace(' ', '/').replace(':', '/')).split("/");
				Integer[]fechaFin2 = new Integer[tamano];

				for(int i = 0; i< fechaFin2.length; i++)
				{
					fechaInicio2[i] = Integer.parseInt(fechaInicio[i]);
					fechaFin2[i] = Integer.parseInt(fechaFin[i]);
				}

				LocalDateTime ini = LocalDateTime.of(fechaInicio2[2], fechaInicio2[0], fechaInicio2[1],fechaInicio2[3],fechaInicio2[4],(tamano==6 ? fechaInicio2[5]:00));
				LocalDateTime fini = LocalDateTime.of(fechaFin2[2], fechaFin2[0], fechaFin2[1],fechaFin2[3],fechaFin2[4],tamano==6 ? fechaFin2[5]:00);
				if(line[10]==null || line[10].equals(""))
				{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), Trip.UNKNOWN);

				}
				else{
					t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
							Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), line[10]);

				}
				linkTrips.add(t);
				line = br.readNext();
			}
			br.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Permite cargar una muestra de los viajes existentes en un periodo de tiempo, incluyendo los l�mites
	 * @param fechaInicial fecha inicial del periodo de tiempo
	 * @param fechaFinal fecha final del periodo de tiempo
	 * @return Lista encadenada con viajes dentro de un periodo de tiempo.
	 */ 
	public static LinkedList<Trip> ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		LocalDateTime inicio = fechaInicial.minusNanos(1);
		LocalDateTime fin = fechaFinal.plusNanos(1);
		LinkedList<Trip> r = new LinkedList<Trip>();
		ListIterator<Trip> listIte = (ListIterator<Trip>) linkTrips.iterator();
		while(listIte.hasNext())
		{
			Trip actual = listIte.next();
			LocalDateTime inicioActual = actual.getStartTime();
			LocalDateTime finActual = actual.getStopTime();
			if(inicioActual.isAfter(inicio)&&finActual.isBefore(fin))
			{
				r.add(actual);
			}
		}
		return r; 

	}
	
	/**
	 * Carga una lista de bicicletas utilizando una lista de viajes proporcionada
	 * @param tList Lista de viajes a partir de la cual se cargar�n las bicicletas
	 */
	public static LinkedList<Bike> cargarBikes(LinkedList<Trip> tList)
	{
		ListIterator<Trip> listIte = (ListIterator<Trip>) tList.iterator();
		Trip t = listIte.next();
		int distance = (int) calcularDistancia(t.getStartStationId(), t.getEndStationId());
		Bike bici = new Bike(t.getBikeId(),distance);
		listaBikes.add(bici);

		while(listIte.hasNext()) {
			t = listIte.next();
			if(t.getBikeId()==bici.getBikeId()) {
				bici.addDistance(t.getDistance());
			}

			else {
				bici = new Bike(t.getBikeId(),distance);
				listaBikes.add(bici);
			}
		}
		return listaBikes;
	}
	
	//REQUERIMIENTOS FUNCIONALES
	
	public void crearColaP(LocalDateTime fInicial, LocalDateTime fFinal)
	{
		ManejadorBicicletas.crearColaP(fInicial, fFinal);
	}
	
	public void crearMonticuloCP(LocalDateTime fInicial, LocalDateTime fFinal) {
		ManejadorBicicletas.crearMonticuloCP(fInicial, fFinal);
	}
	
	public static double calcularDistancia(int inicio, int end)
	{
		boolean ini = false;
		boolean fini = false;
		boolean yaTodo = false;
		Station referencia = null;
		Station ended = null;
		System.out.println(listaEncadenadaStations.size());
		for(int i = 0; i<listaEncadenadaStations.size()&&!yaTodo; i++)
		{
			
			if(listaEncadenadaStations.get(i).getItem().getStationId()==inicio)
			{
				referencia = listaEncadenadaStations.get(i).getItem();
				ini = true;
			}
			if(listaEncadenadaStations.get(i).getItem().getStationId()==end)
			{
				ended = listaEncadenadaStations.get(i).getItem();
				fini = true;
			}
			if(ini&&fini){
				yaTodo = true;
			}
		}

		if(referencia != null && ended != null)
		{
			return calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}
		else {
			return 0;
		}
	}




	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public static double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}



}
