package model.data_structures;

public class OrderedArrayMaxPQ<T extends Comparable<T>> {
	
	/**
	 * arreglo que representa la cola de prioridad
	 */
	private T[] array;
	
	/**
	 * tamanio del arreglo
	 */
	private int size;

	
	//CONSTRUCTOR
	
	/**
	 * Crea un arreglo vacio de tamanio capacity
	 * @param capacity Tamanio del arreglo
	 */
	public OrderedArrayMaxPQ(int capacity) {
		array = (T[]) (new Comparable[capacity]);
		size=0;
	}
	
	//METODOS RETURN
	
	/**
	 * retorna true si la cola de prioridad esta vacia, false de lo contrario
	 * @return true si la cola de prioridad esta vacia, false de lo contrario
	 */
	public boolean isEmpty() {return size==0;}
	
	/**
	 * retorna el numero de elementos en la cola de prioridad
	 * @return numero de elementos en la cola de prioridad
	 */
	public int size() {return size;}
	
	/**
	 * Retorna el elemento con la llave mas grande del arreglo, ubicado en size-1
	 * @return Elemento con la llave mas grande del arreglo
	 */
	public T deleteMax() {return array[--size];}
	
	//METODOS FUNCIONALES
	
	/**
	 * Inserta un elemento en la cola de prioridad
	 * @param t Elemento a insertar
	 */
	public void insert(T t) {
		int i = size-1;
		while(i>=0 && less(t,array[i])) {
			array[i+1]=array[i];
			i--;
		}
		
		array[i+1] = t;
		size++;
	}
	
	public T get(int pos)
	{
		return array[pos];
	}
	//METODOS AUXILIARES
	
	/**
	 * Indica si el elemento v es menor al elemento w
	 * @param v Elemento principal
	 * @param w Elemento a comparar
	 * @return True si v<w, false de lo contrario	
	 */
	public boolean less(T v, T w) {
		return v.compareTo(w) < 0;
	}
	
	
}
