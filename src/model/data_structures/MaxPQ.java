 package model.data_structures;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MaxPQ<T> implements Iterable<T> {
	
	//ATRIBUTOS
	
	/**
	 * arreglo con elementos de indices 1-size
	 */
	private T[] array;
	
	/**
	 * numero de elementos en la cola de prioridad
	 */
	private int size;
	
	/**
	 * comparador opcional
	 */
	private Comparator<T> comparator;

	
	//CONSTRUCTORES
	
	/**
	 * Crea una cola de prioridad vacia con una capacidad inicial dada
	 * @param initialCapacity Capacidad inicial de la cola de prioridad
	 */
	public MaxPQ(int initialCapacity) {
		array = (T[]) new Object[initialCapacity+1];
		size=0;
	}
	
	/**
	 * Crea una cola de prioridad vacia
	 */
	public MaxPQ() {
		this(1);
	}
	
	/**
	 * Crea una cola de prioridad usando el tamanio y el comparador dado
	 * @param initialCapacity Tamanio de la cola de prioridad
	 * @param comparator Comparador que permite comparar las llaves en un orden especifico
	 */
	public MaxPQ(int initialCapacity, Comparator<T> comparator) {
		this.comparator=comparator;
		array = (T[]) new Object[initialCapacity+1];
		size=0;
	}
	
	/**
	 * Crea una cola de prioridad vacia utilizando el comparador dado
	 * @param comparator Comparador que permite comparar las llaves en un orden especifico
	 */
	public MaxPQ(Comparator<T> comparator) {
		this(1, comparator);
	}
	
	/**
	 * Crea una cola de prioridad a partir de un arreglo de llaves recibido
	 * @param llaves Arreglo de llaves
	 */
	public MaxPQ(T[] llaves) {
		size = llaves.length;
		array = (T[]) new Object[llaves.length+1];
		
		for(int i =0; i<size; i++)
			array[i+1] = llaves[i];
		for(int k = size/2;k>=1;k--)
			sink(k);
	}
	
	//METODOS RETURN
	
	/**
	 * retorna true si la cola de prioridad esta vacia, false de lo contrario
	 * @return true si la cola de prioridad esta vacia, false de lo contrario
	 */
	public boolean isEmpty() {return size==0;}
	
	/**
	 * retorna el numero de elementos en la cola de prioridad
	 * @return numero de elementos en la cola de prioridad
	 */
	public int size() {return size;}
	
	/**
	 * Retorna el elemento con la llave mas grande del arreglo, ubicado en la primera posicion del arreglo
	 * @return
	 */
	public T max() {
		if(isEmpty()) throw new NoSuchElementException("No hay elementos en la cola");
		return array[1];
	}
	
	//METODOS FUNCIONALES
	
	/**
	 * Inserta un elemento en la cola de prioridad
	 * @param t Elemento a insertar
	 */
	public void insert(T t) {
		if(size==array.length-1) resize(2*array.length);
		
		array[++size] = t;
		swim(size);
	}
	
	/**
	 * Retorna el elemento con la llave mas grande de la cola de prioridad
	 * @return Elemento mas grande de la cola
	 */
	public T deleteMax() {
		if(isEmpty()) throw new NoSuchElementException("No hay elementos en la cola");
		
		T max = array[1];
		exchange(1,size--);
		sink(1);
		array[size+1] = null;
		if((size>0) && (size==(array.length-1)/4)) resize(array.length/2);
		return max;
	}
	
	//METODOS AUXILIARES
	
	/**
	 * Me permite aumentar el tamanio del arreglo a un valor dado
	 * @param x Valor a aumentar el tamanio del arreglo
	 */
	private void resize(int x) {
		T[] temp = (T[]) new Object[x];
		for(int i=1;i<=size;i++)
			temp[i] = array[i];
				
		array = temp;
	}
	
	/**
	 * Ubica un elemento desde abajo hacia arriba
	 * @param k posicion del elemento a ubicar
	 */
	private void swim(int k) {
		while(k>1 && less(k/2, k)) {
			exchange(k,k/2);
			k=k/2;
		}
	}
	
	/**
	 * Ubica un elemento desde arriba hacia abajo
	 * @param k posicion del elemento a ubicar
	 */
	private void sink(int k) {
		while(2*k<=size) {
			int j = 2*k;
			if(j<size && less(j, j+1))	j++;
			if(!less(k,j)) break;
			exchange(k,j);
			k=j;
		}
	}
	
	/**
	 * Indica si un elemento en i es menor a un elemento en j
	 * @param i Posicion del elemento a ser comparado
	 * @param j Posicion del elemento a comparar
	 * @return true si array[i]<array[j], false de lo contrario
	 */
	private boolean less(int i, int j) {
        if (comparator == null) {
            return ((Comparable<T>) array[i]).compareTo(array[j]) < 0;
        }
        else {
            return comparator.compare(array[i], array[j]) < 0;
        }
    }

	/**
	 * Intercambia un elemento en la posicion i pr un elemento en la posicion j
	 * @param i Posicion del elemento 1
	 * @param j Posicion del elemento 2
	 */
    private void exchange(int i, int j) {
        T swap = array[i];
        array[i] = array[j];
        array[j] = swap;
    }
    
    //ITERATOR
    
    /**
     * Retorna un iterador que itera sobre las llaves en orden descendiente
     */
    public Iterator<T> iterator() {
        return new HeapIterator();
    }
    
    /**
     * Clase para el iterador de un Heap
     */
    private class HeapIterator implements Iterator<T> {

        private MaxPQ<T> copy;

        public HeapIterator() {
            if (comparator == null) copy = new MaxPQ<T>(size());
            else                    copy = new MaxPQ<T>(size(), comparator);
            for (int i = 1; i <= size; i++)
                copy.insert(array[i]);
        }

        public boolean hasNext()  { return !copy.isEmpty();                     }
        public void remove()      { throw new UnsupportedOperationException();  }

        public T next() {
            if (!hasNext()) throw new NoSuchElementException();
            return copy.deleteMax();
        }
    }
    
    public T get(int pos)
	{
		return array[pos];
	}
}
