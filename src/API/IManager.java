package API;

import java.time.LocalDateTime;

import model.data_structures.LinkedList;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public interface IManager {
    
	/**
	 * Carga la informacion de los viajes en Q3 y Q4
	 * @param rutaTrips
	 */
	void cargar(String rutaTrips);
	
	LinkedList<Trip> getTrips();

    void cargarN(String rutaTrips, int n);

	void cargarStations();
}
