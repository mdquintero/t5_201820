package test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import controller.Controller;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.MaxPQ;
import model.data_structures.OrderedArrayMaxPQ;
import model.logic.Manager;
import model.vo.Trip;


public class PruebaColaPrioridad {
	private Manager mundo=new Manager();
	private OrderedArrayMaxPQ<PruebaVO> ordenada;
	private MaxPQ<PruebaVO> max;
	
	
	/**
	 * Primer caso en donde las listas est�n vac�as inicialmente
	 */
	public void setUp1()
	{
		ordenada = new OrderedArrayMaxPQ<PruebaVO>(0);
		max = new MaxPQ<PruebaVO>(30);
		mundo.cargarN(Manager.TRIPS_Q3,40);
	}
	
	
	/**
	 * Segundo caso, las listas se cargan con un numero especifico de datos
	 */
	public void setUp2()
	{
		ordenada = new OrderedArrayMaxPQ<PruebaVO>(50000);
		max = new MaxPQ<PruebaVO>(50000);
		mundo.cargar(Manager.TRIPS_Q3);
	}
	/**
	 * Tercer caso, se cargan con un numero especifico y se llenan
	 */
	public void setUp3()
	{
		mundo.cargarN(Manager.TRIPS_Q3,40);
		ordenada = new OrderedArrayMaxPQ<PruebaVO>(50000);
		max = new MaxPQ<PruebaVO>(50000);
		LinkedList<Trip> lista = mundo.getTrips();
		ListIterator<Trip> ite = lista.iterator();
		while(ite.hasNext())
		{
			Trip t = ite.next();
			PruebaVO n = new PruebaVO(t.getEndStationId(), t.getEndStationId(), t.getTripDuration());
			ordenada.insert(n);
			max.insert(n);
		}
		
	}
	/**
	 * Se cargan escenarios 1 y 2 y se cargan informaciones de la lista y se verifica que estan ordenados por prioridad
	 */
	@Test
	public void add()
	{
		LinkedList<Trip> lista = mundo.getTrips();
		ListIterator<Trip> ite = lista.iterator();
		setUp1();
		while(ite.hasNext())
		{
			Trip t = ite.next();
			PruebaVO n = new PruebaVO(t.getEndStationId(), t.getEndStationId(), t.getTripDuration());
			ordenada.insert(n);
			
			max.insert(n);
			
		}
		PruebaVO actual = null;
		for(int i = 1; i<max.size(); i++)
		{
			actual = max.get(i);
			assertTrue(actual.compareTo(max.get(2*i))==0||actual.compareTo(max.get(2*i))>0);
			assertTrue(actual.compareTo(max.get(2*i+1))==0||actual.compareTo(max.get(2*i+1))>0);
			
		}
		max=null;
		
		
		for(int i = 1; i<ordenada.size(); i++)
		{
			actual = ordenada.get(i);
			assertTrue(actual.compareTo(ordenada.get(i+1))==0||actual.compareTo(ordenada.get(i+1))<0);
		}
		ordenada=null;
		
		setUp2();
		while(ite.hasNext())
		{
			Trip t = ite.next();
			PruebaVO n = new PruebaVO(t.getEndStationId(), t.getEndStationId(), t.getTripDuration());
			ordenada.insert(n);
			max.insert(n);
		}
		
		actual=null;
		for(int i = 1; i<max.size(); i++)
		{
			actual = max.get(i);
			assertTrue(actual.compareTo(max.get(2*i))==0||actual.compareTo(max.get(2*i))>0);
			assertTrue(actual.compareTo(max.get(2*i+1))==0||actual.compareTo(max.get(2*i+1))>0);
		}
		max=null;
		
		actual = ordenada.get(1);
		for(int i = 1; i<ordenada.size(); i++)
		{
			actual = ordenada.get(i);
			assertTrue(actual.compareTo(ordenada.get(i+1))==0||actual.compareTo(ordenada.get(i+1))<0);
		}
		ordenada=null;

	}
	
	/**
	 * Se carga el escenario 2 se llenan las listas y luego se prueba si borran el maximo correctamente manteniendo el orden
	 */
	@Test
	public void delete()
	{
		
		LinkedList<Trip> lista = mundo.getTrips();
		ListIterator<Trip> ite = lista.iterator();
		setUp3();
		while(ite.hasNext())
		{
			Trip t = ite.next();
			PruebaVO n = new PruebaVO(t.getEndStationId(), t.getEndStationId(), t.getTripDuration());
			ordenada.insert(n);
			max.insert(n);
		}
		
		
		PruebaVO maxim = max.deleteMax();

		for(int i=1; i<max.size();i++)
		{
			assertTrue(maxim.compareTo(max.get(i))==0||maxim.compareTo(max.get(i))>0);
			assertTrue(maxim.compareTo(max.get(i*2))==0||maxim.compareTo(max.get(i*2))>0);
			assertTrue(maxim.compareTo(max.get(i*2+1))==0||maxim.compareTo(max.get(i*2+1))>0);
		}
		max=null;
		
		maxim = ordenada.deleteMax();
		for(int i=1; i<ordenada.size();i++)
		{
			assertTrue(maxim.compareTo(ordenada.get(i))==0|maxim.compareTo(ordenada.get(i))>0);
		}
		ordenada =null;
	}
}