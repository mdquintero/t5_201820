package test;

import model.data_structures.OrderedArrayMaxPQ;

public class PruebaVO implements Comparable<PruebaVO> {
	
	
	private String estacionDeInicio;

	private String estacionFinal;
	
	private Double duracionViaje;
	
	/**
	 * Da el atributo estacionDeInicio
	 * @return estacionDeInicio
	 */
	public String getInicio()
	{
		return estacionDeInicio;
	}
	/**
	 * Da el atributo estacionFinal
	 * @return estacionFinal
	 */
	public String getFinal()
	{
		return estacionFinal;
	}
	/**
	 * Da el atributo duracionViaje
	 * @return duracionViaje
	 */
	public double getDuracion()
	{
		return duracionViaje;
	}
	
	public PruebaVO(int inicio, int fin, double duration)
	{
		estacionDeInicio = Integer.toString(inicio);
		estacionFinal = Integer.toString(fin);
		duracionViaje = duration;
	}
	
	/**
	 * Compara con un objeto tipo PruebaVO, primero se compara con la estacion de inicio, luego con la de fin y finalmente con la 
	 * duraci�n del viaje
	 * @param PruebaVO o, objeto con el que se compara el actual
	 * @return -1 en caso de que el actual sea menor, 1 en caso de ser mayor y 0 en caso de ser iguales.
	 */
	@Override
	public int compareTo(PruebaVO o) {
		int compared = this.estacionDeInicio.compareTo(o.getInicio());
		if(this.estacionDeInicio.compareTo(o.getInicio())==0)
		{
			compared = this.estacionFinal.compareTo(o.getFinal());
			if(this.estacionFinal.compareTo(o.getFinal())==0)
			{
				compared = this.duracionViaje.compareTo(o.getDuracion());
			}
		}
		
		return compared/(compared*(-1));
	}
	
}
